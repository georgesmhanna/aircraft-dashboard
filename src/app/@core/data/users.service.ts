import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";

let counter = 0;

@Injectable()
export class UserService {

   API_URL = environment.apiUrl+'/users/';

  // private users = {
  //   nick: { name: 'Nick Jones', picture: 'assets/images/nick.png' },
  //   eva: { name: 'Eva Moor', picture: 'assets/images/eva.png' },
  //   jack: { name: 'Jack Williams', picture: 'assets/images/jack.png' },
  //   lee: { name: 'Lee Wong', picture: 'assets/images/lee.png' },
  //   alan: { name: 'Alan Thompson', picture: 'assets/images/alan.png' },
  //   kate: { name: 'Kate Martinez', picture: 'assets/images/kate.png' },
  // };


  private userArray: any[];

  constructor(private http: HttpClient) {
    // this.userArray = Object.values(this.users);
  }

  getUsers(): Observable<any> {
    return this.http.get(this.API_URL).catch(this.handleError);
  }

  public getUserById(id): Observable<any>{
    return this.http.get(this.API_URL+id)
      .catch(this.handleError);
  }

  public updateUser(id, data):Observable<any>{
    return this.http.put(this.API_URL+id, data)
      .catch(this.handleError);
  }

  public deleteUser(id):Observable<any>{
    return this.http.delete(this.API_URL+id)
      .catch(this.handleError);
  }

  public createUser(data):Observable<any>{
    return this.http.post(this.API_URL, data)
      .catch(this.handleError);
  }

  getUserArray(): Observable<any[]> {
    return Observable.of(this.userArray);
  }

  getUser(): Observable<any> {
    return this.http.get(`${this.API_URL}/users/5af560c6fbc5c853f8f37364`);
  }

  private handleError (error: Response | any) {
    console.error('ApiService::handleError', error);
    return Observable.throw(error);
  }
}
