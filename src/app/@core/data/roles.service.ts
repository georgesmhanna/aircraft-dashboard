import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class RolesService {

  API_URL = environment.apiUrl + '/roles/';

  constructor(private http: HttpClient) {

  }

  public getRoles(): Observable<any>{
    return this.http.get(this.API_URL)
      .catch(this.handleError);
  }

  public getRoleById(id): Observable<any>{
    return this.http.get(this.API_URL+id)
      .catch(this.handleError);
  }

  public updateRole(id, data):Observable<any>{
    return this.http.put(this.API_URL+id, data)
      .catch(this.handleError);
  }

  public deleteRole(id):Observable<any>{
    return this.http.delete(this.API_URL+id)
      .catch(this.handleError);
  }

  public createRole(data):Observable<any>{
    return this.http.post(this.API_URL, data)
      .catch(this.handleError);
  }


  private handleError (error: Response | any) {
    console.log('ApiService::handleErro', error);
    return Observable.throw(error);
  }
}
