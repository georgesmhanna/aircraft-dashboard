import {Injectable} from '@angular/core';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import * as XlsxPop from 'xlsx-populate';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable()
export class ExcelService {

  constructor() {
  }

  public exportAsExcelFile(json: any[], excelFileName: string, cols: any[]): void {
    let worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    worksheet["!cols"] = cols;
    const workbook: XLSX.WorkBook = {Sheets: {'data': worksheet}, SheetNames: ['data']};
    const excelBuffer: any = XLSX.write(workbook, {bookType: 'xlsx', type: 'array'});
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  public exportAsExcelFile2(jsonOpened: any[], jsonClosed: any[], history, excelFileName: string, cols: any[], withHistory = false): void {
    let worksheetOpened: XLSX.WorkSheet = XLSX.utils.json_to_sheet(jsonOpened);
    let worksheetClosed: XLSX.WorkSheet = XLSX.utils.json_to_sheet(jsonClosed);
    let worksheetHistory: XLSX.WorkSheet;
    const workbook = XLSX.utils.book_new();

    worksheetOpened["!cols"] = cols;
    worksheetClosed["!cols"] = cols;
    if (withHistory) {
      worksheetHistory = XLSX.utils.json_to_sheet(history);
      worksheetHistory["!cols"] = cols;
      XLSX.utils.book_append_sheet(workbook, worksheetHistory, 'History');
    }
    // const workbook: XLSX.WorkBook = {Sheets: {'opened': worksheetOpened}, SheetNames: ['open']};
    XLSX.utils.book_append_sheet(workbook, worksheetOpened, 'Opened Today');
    XLSX.utils.book_append_sheet(workbook, worksheetClosed, 'Closed Today');
    const excelBuffer: any = XLSX.write(workbook, {bookType: 'xlsx', type: 'array'});
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});
    XlsxPop.fromDataAsync(data)
      .then(workbook => {
        for (let i = 0; i < workbook._sheets.length; i++) {
          workbook.sheet(i).row(1).style("bold", true);
          workbook.sheet(i).column(1).style("horizontalAlignment", "left");
        }
        workbook.outputAsync().then(blob => {
          FileSaver.saveAs(blob, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
        })
      });
  }
}
