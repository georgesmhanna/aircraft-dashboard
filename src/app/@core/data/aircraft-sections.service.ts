import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class AircraftSectionsService {

  API_URL = environment.apiUrl + '/aircraftsections/';

  constructor(private http: HttpClient) {

  }

  public getAircraftSections(): Observable<any>{
    return this.http.get(this.API_URL)
      .catch(this.handleError);
  }

  public getAircraftSectionById(id): Observable<any>{
    return this.http.get(this.API_URL+id)
      .catch(this.handleError);
  }

  public updateAircraftSection(id, data):Observable<any>{
    return this.http.put(this.API_URL+id, data)
      .catch(this.handleError);
  }

  public deleteAircraftSection(id):Observable<any>{
    return this.http.delete(this.API_URL+id)
      .catch(this.handleError);
  }

  public createAircraftSection(data):Observable<any>{
    return this.http.post(this.API_URL, data)
      .catch(this.handleError);
  }

  public getImage64(name):Observable<any>{
    return this.http.get(this.API_URL+'getImage64/'+name)
      .catch(this.handleError);
  }


  private handleError (error: Response | any) {
    console.error('ApiService::handleError', error);
    return Observable.throw(error);
  }
}
