import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Observable";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";

@Injectable()
export class DefectProfilesService {
  API_URL = environment.apiUrl + '/defectprofiles/';
  constructor(private http: HttpClient) {

  }

  public getDefectProfiles(): Observable<any>{
    return this.http.get(this.API_URL)
      .catch(this.handleError);
  }

  public getDefectProfileById(id): Observable<any>{
    return this.http.get(this.API_URL+id)
      .catch(this.handleError);
  }

  public updateDefectProfile(id, data):Observable<any>{
    return this.http.put(this.API_URL+id, data)
      .catch(this.handleError);
  }

  public deleteDefectProfile(id):Observable<any>{
    return this.http.delete(this.API_URL+id)
      .catch(this.handleError);
  }

  public createDefectProfile(data):Observable<any>{
    return this.http.post(this.API_URL, data)
      .catch(this.handleError);
  }


  private handleError (error: Response | any) {
    console.error('ApiService::handleError', error);
    return Observable.throw(error);
  }
}

