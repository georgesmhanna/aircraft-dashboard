import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { NbAuthService } from '@nebular/auth';
import { tap } from 'rxjs/operators/tap';

@Injectable()
export class AuthGuardService implements CanActivate {
isValidToken: boolean;
  constructor(private authService: NbAuthService, private router: Router) {
  }

  canActivate() {
    this.authService.getToken().subscribe(token => {
      console.log('token:isValid: ', token.isValid());
      this.isValidToken = token.isValid()
    });

    return this.authService.isAuthenticated()
      .pipe(
        tap(authenticated => {
          if (!authenticated || !this.isValidToken) {
            this.router.navigate(['auth/login']);
          }
        }),
      );

  }
}
