import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";

@Injectable()
export class AircraftsService {

  API_URL = environment.apiUrl+'/aircrafts/';
  constructor(private http: HttpClient) { }

  getAircrafts(): Observable<any> {
    return this.http.get(this.API_URL).catch(this.handleError);
  }

  public getAircraftById(id): Observable<any>{
    return this.http.get(this.API_URL+id)
      .catch(this.handleError);
  }

  public updateAircraft(id, data):Observable<any>{
    return this.http.put(this.API_URL+id, data)
      .catch(this.handleError);
  }

  public deleteAircraft(id):Observable<any>{
    return this.http.delete(this.API_URL+id)
      .catch(this.handleError);
  }

  public createAircraft(data):Observable<any>{
    return this.http.post(this.API_URL, data)
      .catch(this.handleError);
  }

  private handleError (error: Response | any) {
    console.error('ApiService::handleError', error);
    return Observable.throw(error);
  }
}
