import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserService } from './users.service';
import { ElectricityService } from './electricity.service';
import { StateService } from './state.service';
import { SmartTableService } from './smart-table.service';
import { PlayerService } from './player.service';
import {RolesService} from './roles.service';
import {AircraftModelsService} from "./aircraft-models.service";
import {AircraftsService} from "./aircrafts.service";
import {DefectProfilesService} from "./defect-profiles.service";
import {DefectTypesService} from "./defect-types.service";
import {AircraftSectionsService} from "./aircraft-sections.service";
import {DefectsService} from "./defects.service";
import {AuthGuardService} from "./auth-guard.service";
import {ExcelService} from "./excel.service";

const SERVICES = [
  UserService,
  ElectricityService,
  StateService,
  SmartTableService,
  PlayerService,
  RolesService,
  AircraftModelsService,
  AircraftsService,
  DefectProfilesService,
  DefectTypesService,
  AircraftSectionsService,
  DefectsService,
  AuthGuardService,
  ExcelService
];

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class DataModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: DataModule,
      providers: [
        ...SERVICES,
      ],
    };
  }
}
