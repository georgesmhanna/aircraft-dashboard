import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs/Observable";

@Injectable()
export class AircraftModelsService {


  API_URL = environment.apiUrl + '/aircraftmodels/';
  roles: any[];

  constructor(private http: HttpClient) {

  }

  public getAircraftModels(): Observable<any> {
    return this.http.get(this.API_URL)
      .catch(this.handleError);
  }

  public getAircraftModelById(id): Observable<any> {
    return this.http.get(this.API_URL + id)
      .catch(this.handleError);
  }

  public updateAircraftModel(id, data): Observable<any> {
    return this.http.put(this.API_URL + id, data)
      .catch(this.handleError);
  }

  public deleteAircraftModel(id): Observable<any> {
    return this.http.delete(this.API_URL + id)
      .catch(this.handleError);
  }

  public createAircraftModel(data): Observable<any> {
    return this.http.post(this.API_URL, data)
      .catch(this.handleError);
  }


  private handleError(error: Response | any) {
    console.error('ApiService::handleError', error);
    return Observable.throw(error);
  }
}


