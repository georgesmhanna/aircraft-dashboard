import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class DefectsService {

  API_URL = environment.apiUrl + '/defects/';

  constructor(private http: HttpClient) {

  }

  public getDefects(): Observable<any>{
    return this.http.get(this.API_URL)
      .catch(this.handleError);
  }

  public getDefectById(id): Observable<any>{
    return this.http.get(this.API_URL+id)
      .catch(this.handleError);
  }

  public updateDefect(id, data):Observable<any>{
    return this.http.put(this.API_URL+id, data)
      .catch(this.handleError);
  }

  public deleteDefect(id):Observable<any>{
    return this.http.delete(this.API_URL+id)
      .catch(this.handleError);
  }

  public createDefect(data):Observable<any>{
    return this.http.post(this.API_URL, data)
      .catch(this.handleError);
  }

  public getCountBySection(id):Observable<any>{
    return this.http.get(this.API_URL+'getCountBySection/'+id)
      .catch(this.handleError);
  }

  public getImage64(name):Observable<any>{
    return this.http.get(this.API_URL+'getImage64/'+name)
      .catch(this.handleError);
  }

  public isImage(id):Observable<any>{
    return this.http.get(this.API_URL+'isImage/'+id)
      .catch(this.handleError);
  }

  public getDefectGroups():Observable<any>{
    return this.http.get(environment.apiUrl+'/getDefectGroups')
      .catch(this.handleError);
  }

  private handleError (error: Response | any) {
    console.error('ApiService::handleError', error);
    return Observable.throw(error);
  }

}
