/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from './@core/utils/analytics.service';
import {NbMenuService} from "@nebular/theme";
import {NbAuthService} from "@nebular/auth";
import {Router} from "@angular/router";
import {ToasterService} from "angular2-toaster";

@Component({
  selector: 'ngx-app',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent implements OnInit {

  constructor(private analytics: AnalyticsService,
              private menuService: NbMenuService,
              private auth: NbAuthService,
              private router: Router,
              private toastService: ToasterService) {
    this.menuService.onItemClick()
      .subscribe((event) => {
        this.onContexttItemSelection(event.item.title);
      });


  }

  ngOnInit(): void {
    this.analytics.trackPageViews();
  }

  onContexttItemSelection(title) {
    setTimeout(()=>{
      this.router.navigate(['auth/login']);
    }, 2000);
    this.router.navigate(['auth/logout']);
    localStorage.removeItem('auth_app_token');
  }
}
