import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { PagesRoutingModule } from './pages-routing.module';
import { ThemeModule } from '../@theme/theme.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { RolesComponent } from './roles/roles.component';
import { UsersComponent } from './users/users.component';
import { AircraftModelsComponent } from './aircraft-models/aircraft-models.component';
import { AircraftsComponent } from './aircrafts/aircrafts.component';
import { DefectProfilesComponent } from './defect-profiles/defect-profiles.component';
import { DefectTypesComponent } from './defect-types/defect-types.component';
import { AircraftSectionsComponent } from './aircraft-sections/aircraft-sections.component';
import { DefectsComponent } from './defects/defects.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import {ToasterModule} from "angular2-toaster";
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { BrowserModule } from '@angular/platform-browser';
import { ModalDialogModule } from 'ngx-modal-dialog';
import {NgxRegisterComponent} from "../@theme/components/auth";
import { UsersCreateComponent } from './users-create/users-create.component';
import {CustomEditorAcComponent} from "./custom-editor-ac/custom-editor-ac.component";
import { CustomRendererBtnComponent } from './custom-renderer-btn/custom-renderer-btn.component';
import {LightboxModule} from "ngx-lightbox";
import { CustomRendererBtnSiComponent } from './custom-renderer-btn-si/custom-renderer-btn-si.component';


const PAGES_COMPONENTS = [
  PagesComponent,
];

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    DashboardModule,
    MiscellaneousModule,
    Ng2SmartTableModule,
    ToasterModule,
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger' // set defaults here
    }),
    ModalDialogModule.forRoot(),
    LightboxModule
],
  entryComponents: [
    CustomEditorAcComponent,
    CustomRendererBtnComponent,
    CustomRendererBtnSiComponent
  ],
  declarations: [
    ...PAGES_COMPONENTS,
    RolesComponent,
    UsersComponent,
    AircraftModelsComponent,
    AircraftsComponent,
    DefectProfilesComponent,
    DefectTypesComponent,
    AircraftSectionsComponent,
    DefectsComponent,
    // NgxRegisterComponent,
    UsersCreateComponent,
    CustomEditorAcComponent,
    CustomRendererBtnComponent,
    CustomRendererBtnSiComponent
  ],
})
export class PagesModule {
}
