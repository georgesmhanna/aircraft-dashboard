import {NbMenuItem} from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  // {
  //   title: 'Welcome',
  //   icon: 'nb-home',
  //   link: '/pages/dashboard',
  //   home: true,
  // },
  {
    title: 'Roles',
    icon: 'fa fa-address-card-o',
    link: '/pages/roles',
  },
  {
    title: 'Users',
    icon: 'fa fa-users',
    link: '/pages/users',
    home: true,
    children: [
      {
        title: 'List',
        link: '/pages/users'
      },
      {
        title: 'Create User',
        link: '/pages/createUser'
      }
    ]
  },
  // {
  //   title: 'Aircraft Models',
  //   icon: 'fa fa-plane',
  //   link: '/pages/aircraft-models',
  // },
  {
    title: 'Aircrafts',
    icon: 'ion-jet',
    link: '/pages/aircrafts',
  },
  {
    title: 'Defect Profiles',
    icon: 'ion-clipboard',
    link: '/pages/defect-profiles',
  },
  {
    title: 'Defect Types',
    icon: 'ion-nuclear',
    link: '/pages/defect-types',
  },
  {
    title: 'Aircraft Sections',
    icon: 'fa fa-map-marker',
    link: '/pages/aircraft-sections',
  },
  {
    title: 'Defects',
    icon: 'fa fa-exclamation-triangle',
    link: '/pages/defects',
  },
  {
    title: 'History',
    icon: 'fa fa-history',
    link: '/pages/history',
  },

];
