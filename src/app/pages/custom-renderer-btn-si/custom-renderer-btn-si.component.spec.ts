import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomRendererBtnSiComponent } from './custom-renderer-btn-si.component';

describe('CustomRendererBtnSiComponent', () => {
  let component: CustomRendererBtnSiComponent;
  let fixture: ComponentFixture<CustomRendererBtnSiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomRendererBtnSiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomRendererBtnSiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
