import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ViewCell} from "ng2-smart-table";

@Component({
  selector: 'custom-renderer-btn-si',
  templateUrl: './custom-renderer-btn-si.component.html',
  styleUrls: ['./custom-renderer-btn-si.component.scss']
})
export class CustomRendererBtnSiComponent implements ViewCell,  OnInit {
  renderValue: string;

  @Input() value: string | number;
  @Input() rowData: any;

  @Output() save: EventEmitter<any> = new EventEmitter();

  constructor() {

  }

  ngOnInit() {
    this.renderValue = this.value.toString().toUpperCase();
  }

  onClick(){
    this.save.emit(this.rowData);
  }

}
