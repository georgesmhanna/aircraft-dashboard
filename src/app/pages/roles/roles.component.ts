import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {RolesService} from '../../@core/data/roles.service';
import {ToasterService} from "angular2-toaster";
import 'style-loader!angular2-toaster/toaster.css';
import {ModalDialogService, SimpleModalComponent} from "ngx-modal-dialog";

@Component({
  selector: 'roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss']
})
export class RolesComponent implements OnInit {

  roles: any;
  settings: any;
  source:any;

  constructor(private roleService: RolesService,
              private toasterService: ToasterService,
              private modalService: ModalDialogService, private viewRef: ViewContainerRef) {
    this.settings = {
      add: {
        addButtonContent: '<i class="fa fa-plus"></i>',
        createButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
        confirmCreate: true
      },
      edit: {
        editButtonContent: '<i class="nb-edit"></i>',
        saveButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
        confirmSave: true
      },
      delete: {
        deleteButtonContent: '<i class="nb-trash" mwlConfirmationPopover></i>',
        confirmDelete: true,
      },
      columns: {

        name: {
          title: 'Name',
          type: 'string',
          width: '90%'
        }
      },
      actions: {
        position: 'right'
      }
    };
    this.source = this.roleService.getRoles();
  }

  ngOnInit() {
    this.roleService.getRoles().subscribe(roles => {
      console.log('get result: ', roles);
      this.roles = roles;
    }, err=>{
      this.toasterService.popAsync('error','Error', 'Could not fetch list of roles. Please try to reload.');
    });
  }


  onCreateConfirm(event: any) {
    console.log(event);
    this.roleService.createRole(event.newData).subscribe(res=>{

      console.log('create result: ', res);
      this.ngOnInit();
      this.toasterService.popAsync('success', `Role ${res.name} successfully created!`);
      event.confirm.resolve();
    }, err=>{
        this.toasterService.popAsync('error', `Error while creating role`, 'Make sure the name is unique!');
        event.confirm.reject();
        return;

    });
  }

  onEditConfirm(event: any) {
    this.roleService.updateRole(event.data._id, event.newData)
      .subscribe(res => {
        console.log('edit result: ', res);
        this.toasterService.popAsync('success', `Role ${res.name} successfully edited!`);
        event.confirm.resolve();
      }, err=>{
        this.toasterService.popAsync('error', `Error while editing role`, 'Make sure the name is unique!');
        event.confirm.reject();
        return;
      })
  }

  onDeleteConfirm(event: any) {
    this.modalService.openDialog(this.viewRef, {
      title: 'Confirm',
      childComponent: SimpleModalComponent,
      data: {
        text: 'Are you sure you want to delete this role?'
      },
      settings: {
        closeButtonClass: 'close theme-icon-close'
      },
      actionButtons: [
        {
          text: 'YES', onAction: () => new Promise((resolve:any) => {
            this.roleService.deleteRole(event.data._id)
              .subscribe(res => {
                console.log('delete result: ', res);
                resolve();
                this.toasterService.popAsync('success', `Role successfully deleted!`);
                event.confirm.resolve();
              }, err=>{
                this.toasterService.popAsync('error', `Error while deleting role`, 'Make sure you are connected to the Internet');
                resolve();
                event.confirm.reject();
                return;
              })
          })
        },
        {text: 'No', onAction: () => new Promise(resolve=>{
          resolve();
          })}
      ],

    });
  }
}
