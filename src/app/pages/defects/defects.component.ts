import {Component, OnInit, ViewContainerRef} from '@angular/core';
import * as moment from "moment";
import * as html2canvas from "html2canvas";
import * as jsPDF from "jspdf";
import {ToasterService} from "angular2-toaster";
import 'style-loader!angular2-toaster/toaster.css';
import {ModalDialogService, SimpleModalComponent} from "ngx-modal-dialog";
import {DefectsService} from "../../@core/data/defects.service";
import {UserService} from "../../@core/data/users.service";
import {AircraftsService} from "../../@core/data/aircrafts.service";
import {DefectTypesService} from "../../@core/data/defect-types.service";
import {AircraftSectionsService} from "../../@core/data/aircraft-sections.service";
import {CustomRendererBtnComponent} from "../custom-renderer-btn/custom-renderer-btn.component";
import {Lightbox} from "ngx-lightbox";
import {ExcelService} from "../../@core/data/excel.service";
import {CustomRendererBtnSiComponent} from "../custom-renderer-btn-si/custom-renderer-btn-si.component";
import {Router} from "@angular/router";


@Component({
  selector: 'defects',
  templateUrl: './defects.component.html',
  styleUrls: ['./defects.component.scss']
})
export class DefectsComponent implements OnInit {

  defects: any;
  settings: any;
  source: any;
  users: any[] = [];
  aircrafts: any[] = [];
  defectTypes: any[] = [];
  sections: any[] = [];
  mode = 'defects';
  private defectImages: boolean[];
  private allDefects: any;

  constructor(private defectsService: DefectsService,
              private  usersService: UserService,
              private aircraftsService: AircraftsService,
              private defectTypesService: DefectTypesService,
              private aircraftsSectionsService: AircraftSectionsService,
              private toastService: ToasterService,
              private modalService: ModalDialogService,
              private viewRef: ViewContainerRef,
              private _lightbox: Lightbox,
              private excelService: ExcelService,
              private router: Router) {
  }

  ngOnInit() {

    this.mode = this.router.url.substr(this.router.url.lastIndexOf('/') + 1);
    this.users = [
      {
        value: '', title: '---', id: null
      }
    ];
    this.usersService.getUsers()
      .subscribe(users => {
        users.forEach(user => {
          this.users.push({
            value: user._id,
            title: `${user.firstName} ${user.lastName}`,
            id: user._id
          });
        });
        this.aircrafts = [];
        this.aircraftsService.getAircrafts()
          .subscribe(aircrafts => {
            aircrafts.forEach(aircraft => {
              this.aircrafts.push({
                value: aircraft._id,
                title: aircraft.registrationNo,
                regno: aircraft.registrationNo,
                type: aircraft.name
              });
            });
            this.defectTypes = [];
            this.defectTypesService.getDefectTypes()
              .subscribe(defectTypes => {
                defectTypes.forEach(defectType => {
                  this.defectTypes.push({
                    value: defectType._id,
                    title: defectType.name
                  });
                });
                this.sections = [];
                this.aircraftsSectionsService.getAircraftSections()
                  .subscribe(aircraftSections => {
                    aircraftSections.forEach(section => {
                      const asAircraft = this.aircrafts.find(x => x.value === section.aircraft);
                      const regNo = asAircraft ? asAircraft.regno : '';
                      this.sections.push({
                        value: section._id,
                        title: section.name,
                        regNo: regNo
                      });
                    });
                    this.settings = {
                      attr: {
                        id: 'defectsTable'
                      },
                      add: {
                        addButtonContent: '<i class="fa fa-plus"></i>',
                        createButtonContent: '<i class="nb-checkmark"></i>',
                        cancelButtonContent: '<i class="nb-close"></i>',
                        confirmCreate: true
                      },
                      edit: {
                        editButtonContent: '<i class="nb-edit"></i>',
                        saveButtonContent: '<i class="nb-checkmark"></i>',
                        cancelButtonContent: '<i class="nb-close"></i>',
                        confirmSave: true
                      },
                      delete: {
                        deleteButtonContent: '<i class="nb-trash"></i>',
                        confirmDelete: true,
                      },
                      columns: {
                        reporter: {
                          title: 'Reporter',
                          type: 'text',
                          editor: {
                            type: 'completer',
                            config: {
                              completer: {
                                data: this.users,
                                searchFields: 'title',
                                titleField: 'title',
                                placeholder: 'Search'
                              }
                            }
                          },
                          filter: {
                            type: 'completer',
                            config: {
                              completer: {
                                data: this.users,
                                searchFields: 'title',
                                titleField: 'title',
                                placeholder: 'Search'
                              }
                            }
                          },
                          valuePrepareFunction: (value) => {
                            if (!value)
                              return '---';
                            if (this.users) {
                              return this.users
                                .filter(user => user.value === value)
                                .map(x => x.title)[0];
                            }
                          },
                          filterFunction: (cellStr, search) => {
                            if (!this.users) return false;
                            return this.users
                              .filter(user => user.value === cellStr)
                              .map(x => x.title)[0] === search;
                          }
                        },
                        reportedDate: {
                          title: 'Action Date',
                          type: 'html',
                          editable: false,
                          filter: false,
                          addable: false,
                          valuePrepareFunction: (value) => {
                            return moment(value).format('MMMM DD YYYY, h:mm:ss a')
                          }
                        },
                        aircraft: {
                          title: 'Aircraft Registration Number',
                          editor: {
                            type: 'list',
                            config: {
                              list: this.aircrafts
                            }
                          },
                          filter: {
                            type: 'list',
                            config: {
                              list: this.aircrafts
                            }
                          },
                          valuePrepareFunction: (value) => {
                            if (this.aircrafts) {
                              return this.aircrafts
                                .filter(aircraft => aircraft.value === value)
                                .map(x => x.title)[0];
                            }
                          }
                        },
                        defectType: {
                          title: 'Defect Type',
                          editor: {
                            type: 'list',
                            config: {
                              list: this.defectTypes
                            }
                          },
                          filter: {
                            type: 'list',
                            config: {
                              list: this.defectTypes
                            }
                          },
                          valuePrepareFunction: (value) => {
                            if (this.defectTypes) {
                              return this.defectTypes
                                .filter(dt => dt.value === value)
                                .map(x => x.title)[0];
                            }
                          }
                        },
                        section: {
                          title: 'Cabin Class',
                          editor: {
                            type: 'list',
                            config: {
                              list: this.sections
                            }
                          },
                          filter: {
                            type: 'list',
                            config: {
                              list: this.sections
                            }
                          },
                          valuePrepareFunction: (value) => {
                            if (this.sections) {
                              return this.sections
                                .filter(section => section.value === value)
                                .map(x => x.title)[0];
                            }
                          }
                        },
                        location: {
                          title: "Location",
                          type: "string"
                        },
                        comment: {
                          title: 'Comment',
                          editor: {
                            type: 'textarea'
                          },
                          type: 'string'
                        },
                        partNumber: {
                          title: 'Part number',
                          type: 'string'
                        },
                        workOrderNumber: {
                          title: 'Work Order Number',
                          type: 'string'
                        },
                        issueDate: {
                          title: 'Issue Date',
                          type: 'string'
                        },
                        closedBy: {
                          title: 'Closed By',
                          editor: {
                            type: 'completer',
                            config: {
                              completer: {
                                data: this.users,
                                searchFields: 'title',
                                titleField: 'title',
                                placeholder: 'Search'
                              }
                            }
                          },
                          filter: {
                            type: 'completer',
                            config: {
                              completer: {
                                data: this.users,
                                searchFields: 'title',
                                titleField: 'title',
                                placeholder: 'Search'
                              }
                            }
                          },
                          valuePrepareFunction: (value) => {
                            if (!value)
                              return '---';
                            if (this.users) {
                              return this.users
                                .filter(user => user.value === value)
                                .map(x => x.title)[0];
                            }
                          },
                          filterFunction: (cellStrCB, searchCB) => {
                            if (!this.users) return false;
                            return this.users
                              .filter(user => user.value === cellStrCB)
                              .map(x => x.title)[0] === searchCB;
                          }
                        },
                        closedDate: {
                          title: 'Closed Date',
                          type: 'html',
                          editable: false,
                          filter: false,
                          addable: false,
                          valuePrepareFunction: (value) => {
                            let returnValue = moment(value).format('MMMM DD YYYY, h:mm:ss a');
                            return returnValue !== 'Invalid date' ? returnValue : '';
                          }
                        },
                        buttonSec: {
                          title: "Section Image",
                          type: 'custom',
                          filter: false,
                          addable: false,
                          editable: false,
                          renderComponent: CustomRendererBtnSiComponent,
                          onComponentInitFunction: (instance) => {
                            instance.save.subscribe(row => {
                              this.defectsService.getCountBySection(row._id)
                                .subscribe(count => {
                                  // Got count.. now let's get aircraft section name:
                                  const aircraftSection = this.sections.find(sec => sec.value === row.section);
                                  let aircraftSectionName = (aircraftSection && aircraftSection.title) ? aircraftSection.title.toLowerCase() : '';
                                  const oldASName = aircraftSectionName; // to preserve original for caption image
                                  const aircraftSectionRegNo = aircraftSection.regNo;
                                  if(aircraftSectionName === 'b/c') aircraftSectionName = 'businessclass';
                                  if(aircraftSectionName === 'e/c') aircraftSectionName = 'economyclass';
                                  if(aircraftSectionName === 'f/c') aircraftSectionName = 'firstclass';
                                  // get base64 image of aircraft section:
                                  this.aircraftsSectionsService.getImage64(aircraftSectionName)
                                    .subscribe(image64 => {
                                      this.openSectionImg(image64, oldASName.toUpperCase(), count, aircraftSectionRegNo);
                                    }, err2 => {
                                      this.toastService.popAsync('error', 'Error', 'Could not find image of aircraft section ' + aircraftSectionName + '. Please try again later.');
                                    })
                                  // alert(`aircraft section name is ${aircraftSectionName} and count is ${count}`);
                                }, err => {
                                  this.toastService.popAsync('error', 'Error', 'Could not fetch number of defects. Please try again later.');
                                })
                            })
                          },
                        },
                        buttonDef: {
                          title: "Defect Image",
                          type: 'custom',
                          filter: false,
                          addable: false,
                          editable: false,
                          renderComponent: CustomRendererBtnComponent,
                          onComponentInitFunction: (instance) => {
                            instance.save.subscribe(row => {
                              this.defectsService.getImage64(row._id)
                                .subscribe(image64 => {
                                  this.openLightBox(image64);
                                }, err => {
                                  this.toastService.popAsync('error', 'Error', 'Image not found. Please try again later.');
                                })
                            })
                          },
                        }
                      },
                      actions: {
                        position: 'right',
                        add: this.mode === 'defects',
                        edit: this.mode === 'defects',
                        delete: this.mode === 'defects',
                      },
                      pager: {
                        perPage: 30
                      }
                    };
                    this.defectsService.getDefects()
                      .subscribe(defects => {
                        this.allDefects = defects;
                        this.defects = (this.mode === 'defects') ? defects.filter(defect => !defect.closedDate) : defects.filter(defect => !!defect.closedDate);
                        this.isImage(this.defects).then(arr => {
                          this.defectImages = arr;
                        });
                      }, err1 => {
                        this.toastService.popAsync('error', 'Error', 'Could not fetch list of defects. Please try to reload.');
                      });
                  }, err2 => {
                    this.toastService.popAsync('error', 'Error', 'Could not fetch list of defect profiles. Please try to reload.');
                  });
              }, err3 => {
                this.toastService.popAsync('error', 'Error', 'Could not fetch list of defect types. Please try to reload.');
              });
          }, err4 => {
            this.toastService.popAsync('error', 'Error', 'Could not fetch list of aircrafts. Please try to reload.');
          })
      }, err5 => {
        this.toastService.popAsync('error', 'Error', 'Could not fetch list of users. Please try to reload.');
      })
  }

  onEditConfirm(event: any) {
    if (!event.data.closedBy && event.newData.closedBy) //  changing status
    {
      event.newData.closedDate = new Date();
    }
    this.defectsService.updateDefect(event.data._id, event.newData)
      .subscribe(res => {
        this.ngOnInit();
        this.toastService.popAsync('success', `Defect successfully edited!`);
        event.confirm.resolve();
      }, err => {
        this.toastService.popAsync('error', `Error while editing defect`, 'Make sure that all information has been filled correctly!');
        event.confirm.reject();
        return;
      })
  }

  onCreateConfirm(event: any) {
    this.defectsService.createDefect(event.newData).subscribe(res => {
      this.ngOnInit();
      this.toastService.popAsync('success', `Defect successfully created!`);
      event.confirm.resolve();
    }, err => {
      this.toastService.popAsync('error', `Error while creating defect`, 'Make sure that all information has been filled correctly!');
      event.confirm.reject();
      return;
    });
  }

  onDeleteConfirm(event: any) {
    this.modalService.openDialog(this.viewRef, {
      title: 'Confirm',
      childComponent: SimpleModalComponent,
      data: {
        text: 'Are you sure you want to delete this Defect?'
      },
      settings: {
        closeButtonClass: 'close theme-icon-close'
      },
      actionButtons: [
        {
          text: 'YES', onAction: () => new Promise((resolve: any) => {
            this.defectsService.deleteDefect(event.data._id)
              .subscribe(res => {
                resolve();
                this.ngOnInit();
                this.toastService.popAsync('success', `Defect successfully deleted!`);
                event.confirm.resolve();
              }, err => {
                this.toastService.popAsync('error', `Error while deleting Defect`, 'Make sure you are connected to the Internet');
                resolve();
                event.confirm.reject();
                return;
              })
          })
        },
        {
          text: 'No', onAction: () => new Promise(resolve => {
            resolve();
          })
        }
      ],
    });
  }

  openLightBox(image64) {
    const album = {
      src: image64,
      caption: "Defect"
    };
    let albums = [];
    albums.push(album);

    this._lightbox.open(albums, 0);
  }

  openSectionImg(image64, aircraftSectionName, count, regno) {
    // Get the modal
    let modal = document.getElementById('myModal');

// Get the image and insert it inside the modal - use its "alt" text as a caption
    let modalImg = <HTMLImageElement> document.getElementById("image");
    modalImg.src = image64;

    let captionText = document.getElementById("caption");

    modal.style.display = "block";

    captionText.innerHTML = `Aircraft Section: ${aircraftSectionName}. <br> Number of Defects: ${count} <br>
    Registration Number: ${regno}`;

    modalImg.onload = function () {

      const width = modalImg.width;
      const height = modalImg.height;

      let canvas = <HTMLCanvasElement> document.getElementById("canvas");
      canvas.style.width = `${width}px`;
      canvas.style.height = `${height}px`;

      canvas.width = width;
      canvas.height = height;
      canvas.style.position = 'absolute';
      canvas.style.left = `${modalImg.offsetLeft}px`;
      canvas.style.top = `${modalImg.offsetTop}px`;
      canvas.style.zIndex = '100000';
      canvas.style.pointerEvents = 'none';

      let ctx = canvas.getContext("2d");
      // ctx.clearRect(0, 0, width, height);

      // ctx.drawImage(modalImg, 0, 0, width, height);
      for (let i = 1; i <= count; i++) {
        let y = height / 3 + Math.floor(Math.random() * (height / 1.5));
        let x = width / 6 + Math.floor(Math.random() * width / 2.5);
        const radius = 10;
        ctx.beginPath();
        ctx.arc(x, y, radius, Math.PI * 2, 0, false);
        ctx.fillStyle = "red";
        ctx.fill();
        ctx.closePath();
      }
    };
  }

  closeModal() {
    let modal = document.getElementById('myModal');
    modal.style.display = "none";
  }

  downloadSectionImage() {
    let modalImg = <HTMLImageElement> document.getElementById("image");

    html2canvas(document.querySelector("#globalCanvas")).then(canvas => {
      let a = document.createElement('a');
      document.body.appendChild(a);
      a.download = "section.png";
      a.href = canvas.toDataURL();
      // a.click();

      // var imgData = canvas.toDataURL("image/jpeg", 1.0);
      // console.log('imgData: ', a.href)
      var pdf = new jsPDF("l", "mm", "a1");

      pdf.addImage(canvas, 'JPEG', 0, 0);
      pdf.save(`section.pdf`);

    });
  }

  exportToExcel() {
    const excelData = this.generateExcelData();
    const tableArr = excelData[0];
    const dailyOpened = excelData[1];
    const dailyClosed = excelData[2];
    const history = excelData[3];

    const cols: any[] = [
      {wpx: 200}, // reported date
      {wpx: 100}, // aircraft type
      {wpx: 100}, //reg no
      {wpx: 120}, //wo no
      {wpx: 100}, //part no
      {wpx: 30}, // area
      {wpx: 60}, // location
      {wpx: 60}, // picture
      {wpx: 250}, // defect type
      {wpx: 60}, // fixed
      {wpx: 100}, // reported by
      {wpx: 100}, // closed by
    ];

    this.excelService.exportAsExcelFile(tableArr, "defects", cols);

    // generate 2nd excel:

    this.defectsService.getDefectGroups().subscribe(arr => {
      const cols2: any[] = [
        {wpx: 40}, // week
        {wpx: 80}, // aircraft
        {wpx: 50}, // opened
        {wpx: 50}, // closed
      ];
      this.excelService.exportAsExcelFile(arr, "aircrafts", cols2);
    });

    // generate 3rd file:
    // all the defects that were closed today and closed today

    this.excelService.exportAsExcelFile2(dailyOpened, dailyClosed, [], "daily", cols);

    // generate 4th file
    // all the defects opened before that date, as well as opened today and closed today
    this.excelService.exportAsExcelFile2(dailyOpened, dailyClosed, history, "history", cols, true);

  }

  isImage(defects): Promise<boolean[]> {
    return new Promise((resolve, reject) => {
      let defectImages = [];
      defects.forEach(defect => {
        this.defectsService.isImage(defect._id)
          .subscribe(value => {
            defectImages.push(value);
          }, err => {
            defectImages.push(false);
          })
      });
      resolve(defectImages);
    })

  }

  private generateExcelData() {
    let tableOpened = [];
    let tableClosed = [];
    let history = [];
    let tableAll = [];
    let emptyExcelStructure;
    let defectRows = [];
    this.allDefects.forEach((defect, index) => {
      let defectRow = [];
      // action date:
      defectRow.push(moment(defect.reportedDate).format('MMMM DD YYYY, h:mm:ss a'));
      let aircraftType = this.aircrafts
        .filter(aircraft => aircraft.value === defect.aircraft)
        .map(x => x.type)[0];
      // aircraft type
      defectRow.push(aircraftType);
      // registration number
      defectRow.push(this.aircrafts
        .filter(aircraft => aircraft.value === defect.aircraft)
        .map(x => x.regno)[0]);
      // work order number
      defectRow.push(defect.workOrderNumber);
      // part number
      defectRow.push(defect.partNumber);
      const area = this.sections
        .filter(section => section.value === defect.section)
        .map(x => x.title)[0];
      // area
      defectRow.push(area === 'galley' ? 'G' : (area === 'lavatory' ? 'L' : 'C'));
      // location
      defectRow.push(defect.location);
      // picture
      defectRow.push(defect.isImage ? "Yes" : "No");
      // defect type
      defectRow.push(this.defectTypes
        .filter(dt => dt.value === defect.defectType)
        .map(x => x.title)[0]);
      // fixed
      defectRow.push(defect.closedDate ? "Yes" : "No");
      // reported by
      defectRow.push(this.users
        .filter(user => user.value === defect.reporter)
        .map(x => x.title)[0]);
      // closed by
      defectRow.push(this.users
        .filter(user => user.value === defect.closedBy)
        .map(x => x.title)[0] || '---');
      // closed date but not for excel
      defectRow.push(defect.closedDate);
      defectRows.push(defectRow);
      // defectRows[index].push()


    });
    for (let i = 0; i < defectRows.length; i++) {// starting from 3rd row (1st 2 rows are the header and the filter)
      let excelStructure = {
        "Action Date": defectRows[i][0],
        "Aircraft Type": defectRows[i][1],
        "Registration No": defectRows[i][2],
        "Work Order Number": defectRows[i][3],
        "Part Number": defectRows[i][4],
        "Area": defectRows[i][5],
        "Location": defectRows[i][6],
        "Picture": defectRows[i][7],
        "Defect Type": defectRows[i][8],
        "Fixed": defectRows[i][9],
        "Reported By": defectRows[i][10],
        "Closed By": defectRows[i][11],
      };
      emptyExcelStructure = {
        "Action Date": ' ',
        "Aircraft Type": ' ',
        "Registration No": ' ',
        "Work Order Number": ' ',
        "Part Number": ' ',
        "Area": ' ',
        "Location": ' ',
        "Picture": ' ',
        "Defect Type": ' ',
        "Fixed": ' ',
        "Reported By": ' ',
        "Closed By": ' '
      };
      let openedToday = defectRows[i][0] && moment(defectRows[i][0]).isSame(moment(), 'day');
      let closedToday = defectRows[i][12] && moment(defectRows[i][12]).isSame(moment(), 'day');
      let openedBefore = defectRows[i][0] && moment(defectRows[i][0]).isBefore(moment(), 'day') && !defectRows[i][12]; // opened before but not closed

      tableAll.push(excelStructure);
      if (openedToday) {
        tableOpened.push(excelStructure);
      }
      if (closedToday) {
        tableClosed.push(excelStructure)
      }
      if (openedBefore) {
        history.push(excelStructure);
      }
    }
    if (tableAll.length === 0) {
      tableAll.push(emptyExcelStructure);
    }
    if (tableOpened.length === 0) {
      tableOpened.push(emptyExcelStructure);
    }

    if (tableClosed.length === 0) {
      tableClosed.push(emptyExcelStructure);
    }

    if (history.length === 0) {
      history.push(emptyExcelStructure);
    }


    return [tableAll, tableOpened, tableClosed, history];
  }
}
