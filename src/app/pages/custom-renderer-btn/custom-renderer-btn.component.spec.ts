import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomRendererBtnComponent } from './custom-renderer-btn.component';

describe('CustomRendererBtnComponent', () => {
  let component: CustomRendererBtnComponent;
  let fixture: ComponentFixture<CustomRendererBtnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomRendererBtnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomRendererBtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
