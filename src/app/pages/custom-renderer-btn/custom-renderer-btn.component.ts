import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ViewCell} from "ng2-smart-table";

@Component({
  selector: 'custom-renderer-btn',
  templateUrl: './custom-renderer-btn.component.html',
  styleUrls: ['./custom-renderer-btn.component.scss']
})
export class CustomRendererBtnComponent implements ViewCell, OnInit {
  renderValue: string;

  @Input() value: string | number;
  @Input() rowData: any;

  @Output() save: EventEmitter<any> = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
    this.renderValue = this.value.toString().toUpperCase();
  }

  onClick(){
    this.save.emit(this.rowData);
  }



}
