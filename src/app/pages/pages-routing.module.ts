import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';

import {PagesComponent} from './pages.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {NotFoundComponent} from './miscellaneous/not-found/not-found.component';
import {RolesComponent} from './roles/roles.component';
import {UsersComponent} from './users/users.component';
import {AircraftModelsComponent} from './aircraft-models/aircraft-models.component';
import {AircraftsComponent} from './aircrafts/aircrafts.component';
import {DefectProfilesComponent} from './defect-profiles/defect-profiles.component';
import {DefectTypesComponent} from './defect-types/defect-types.component';
import {AircraftSectionsComponent} from './aircraft-sections/aircraft-sections.component';
import {DefectsComponent} from './defects/defects.component';
import {NgxRegisterComponent} from "../@theme/components/auth";
import {UsersCreateComponent} from "./users-create/users-create.component";

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children:
    [
      //     {
      //   path: 'dashboard',
      //   component: DashboardComponent,
      // },
      {
        path: 'roles',
        component: RolesComponent,
      }, {
      path: 'users',
      component: UsersComponent,
    },
      {
        path: 'createUser',
        component: UsersCreateComponent
      }

      // ,  {
      //   path: 'aircraft-models',
      //   component: AircraftModelsComponent,
      // },
      , {
      path: 'aircrafts',
      component: AircraftsComponent,
    }, {
      path: 'defect-profiles',
      component: DefectProfilesComponent,
    }, {
      path: 'defect-types',
      component: DefectTypesComponent,
    }, {
      path: 'aircraft-sections',
      component: AircraftSectionsComponent,
    }, {
      path: 'defects',
      component: DefectsComponent,
    },
      {
        path: 'history',
        component: DefectsComponent,
      },

      {
        path: 'miscellaneous',
        loadChildren: './miscellaneous/miscellaneous.module#MiscellaneousModule',
      }, {
      path: '',
      redirectTo: 'users',
      pathMatch: 'full',
    }, {
      path: '**',
      component: NotFoundComponent,
    }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
