import { Component, OnInit, ViewContainerRef } from '@angular/core';
import * as moment from "moment";
import {ToasterService} from "angular2-toaster";
import 'style-loader!angular2-toaster/toaster.css';
import {ModalDialogService, SimpleModalComponent} from "ngx-modal-dialog";
import {AircraftModelsService} from "../../@core/data/aircraft-models.service";
import {AircraftsService} from "../../@core/data/aircrafts.service";


@Component({
  selector: 'aircrafts',
  templateUrl: './aircrafts.component.html',
  styleUrls: ['./aircrafts.component.scss']
})
export class AircraftsComponent implements OnInit {

  aircrafts: any;
  settings: any;
  source: any;
  models: any[] = [];

  constructor(
    private modelsService: AircraftModelsService,
    private aircraftsService: AircraftsService,
    private toastService: ToasterService,
    private modalService: ModalDialogService,
    private viewRef: ViewContainerRef
  ) { }

  ngOnInit() {
    this.models = [];
    this.modelsService.getAircraftModels()
      .subscribe(models => {
        models.forEach(model => {
          this.models.push({
            value: model._id,
            title: model.name
          });
        });

        this.settings = {
          add: {
            addButtonContent: '<i class="fa fa-plus"></i>',
            createButtonContent: '<i class="nb-checkmark"></i>',
            cancelButtonContent: '<i class="nb-close"></i>',
            confirmCreate: true
          },
          edit: {
            editButtonContent: '<i class="nb-edit"></i>',
            saveButtonContent: '<i class="nb-checkmark"></i>',
            cancelButtonContent: '<i class="nb-close"></i>',
            confirmSave: true
          },
          delete: {
            deleteButtonContent: '<i class="nb-trash"></i>',
            confirmDelete: true,
          },
          columns: {

            name: {
              title: 'Aircraft Type',
              type: 'string',
              width: '35%'
            },

            createdOn: {
              title: 'Created On',
              type: 'html',
              editable: false,
              filter: false,
              addable: false,
              width: '25%',
              valuePrepareFunction: (value) => {
                return moment(value).format('MMMM DD YYYY, h:mm:ss a')
              }
            },
            model: {
              title: 'Aircraft Model',
              width: '15%',
              editor: {
                type: 'list',
                config: {
                  list: this.models
                }
              },
              filter: {
                type: 'list',
                config: {
                  list: this.models
                }
              },
              valuePrepareFunction: (value) => {
                if (this.models) {
                  return this.models
                    .filter(model => model.value === value)
                    .map(x => x.title)[0];
                }
              }
            },
            registrationNo: {
              title: 'Registration Number',
              type: 'string',
              width: '15%'
            }
          },
          actions: {
            position: 'right'
          }
        };
        this.source = this.aircraftsService.getAircrafts()
          .subscribe(aircrafts => {
            this.aircrafts = aircrafts;
          }, err => {
            this.toastService.popAsync('error', 'Error', 'Could not fetch list of aircraft models. Please try to reload.');
          });
      }, err => {
        this.toastService.popAsync('error', 'Error', 'Could not fetch list of aircrafts. Please try to reload.');
      });
  }

  onEditConfirm(event: any) {
    this.aircraftsService.updateAircraft(event.data._id, event.newData)
      .subscribe(res => {
        this.toastService.popAsync('success', `Aircraft ${res.name} successfully edited!`);
        event.confirm.resolve();
      }, err=>{
        this.toastService.popAsync('error', `Error while editing aircraft`, 'Make sure the name is unique!');
        event.confirm.reject();
        return;
      })
  }

  onDeleteConfirm(event: any) {
    this.modalService.openDialog(this.viewRef, {
      title: 'Confirm',
      childComponent: SimpleModalComponent,
      data: {
        text: 'Are you sure you want to delete this aircraft?'
      },
      settings: {
        closeButtonClass: 'close theme-icon-close'
      },
      actionButtons: [
        {
          text: 'YES', onAction: () => new Promise((resolve:any) => {
            this.aircraftsService.deleteAircraft(event.data._id)
              .subscribe(res => {
                console.log('delete result: ', res);
                resolve();
                this.toastService.popAsync('success', `Aircraft successfully deleted!`);
                event.confirm.resolve();
              }, err=>{
                this.toastService.popAsync('error', `Error while deleting aircraft`, 'Make sure you are connected to the Internet');
                resolve();
                event.confirm.reject();
                return;
              })
          })
        },
        {text: 'No', onAction: () => new Promise(resolve=>{
            resolve();
          })}
      ],

    });
  }

  onCreateConfirm(event: any) {
    console.log(event);
    this.aircraftsService.createAircraft(event.newData).subscribe(res=>{
      this.ngOnInit();
      this.toastService.popAsync('success', `Aircraft ${res.name} successfully created!`);
      event.confirm.resolve();
    }, err=>{
      this.toastService.popAsync('error', `Error while creating Aircraft`, 'Make sure the name is unique.');
      event.confirm.reject();
      return;
    });
  }

}
