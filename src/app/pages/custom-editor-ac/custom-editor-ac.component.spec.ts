import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomEditorAcComponent } from './custom-editor-ac.component';

describe('CustomEditorAcComponent', () => {
  let component: CustomEditorAcComponent;
  let fixture: ComponentFixture<CustomEditorAcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomEditorAcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomEditorAcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
