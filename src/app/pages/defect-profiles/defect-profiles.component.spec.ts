import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefectProfilesComponent } from './defect-profiles.component';

describe('DefectProfilesComponent', () => {
  let component: DefectProfilesComponent;
  let fixture: ComponentFixture<DefectProfilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefectProfilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefectProfilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
