import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {ToasterService} from "angular2-toaster";
import 'style-loader!angular2-toaster/toaster.css';
import {ModalDialogService, SimpleModalComponent} from "ngx-modal-dialog";
import {DefectProfilesService} from "../../@core/data/defect-profiles.service";
import {NbAuthService} from "@nebular/auth";

@Component({
  selector: 'defect-profiles',
  templateUrl: './defect-profiles.component.html',
  styleUrls: ['./defect-profiles.component.scss']
})
export class DefectProfilesComponent implements OnInit {

  defectProfiles: any;
  settings: any;
  source:any;

  constructor(private defectProfilesService : DefectProfilesService,
              private toasterService: ToasterService,
              private modalService: ModalDialogService, private viewRef: ViewContainerRef, private auth: NbAuthService) {

auth.isAuthenticated().subscribe(x=>{
  // console.log('georgeeeeeeee', x);
})
    this.settings = {
      add: {
        addButtonContent: '<i class="fa fa-plus"></i>',
        createButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
        confirmCreate: true
      },
      edit: {
        editButtonContent: '<i class="nb-edit"></i>',
        saveButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
        confirmSave: true
      },
      delete: {
        deleteButtonContent: '<i class="nb-trash" mwlConfirmationPopover></i>',
        confirmDelete: true,
      },
      columns: {

        name: {
          title: 'Name',
          type: 'string',
          width: '90%'
        }
      },
      actions: {
        position: 'right'
      }
    };
    this.source = this.defectProfilesService.getDefectProfiles();
  }
  ngOnInit() {
    this.defectProfilesService.getDefectProfiles().subscribe(defectProfiles => {
      this.defectProfiles = defectProfiles;
    }, err=>{
      this.toasterService.popAsync('error','Error', 'Could not fetch list of defect profiles. Please try to reload.');
    });
  }

  onCreateConfirm(event: any) {
    console.log(event);
    this.defectProfilesService.createDefectProfile(event.newData).subscribe(res=>{

      console.log('create result: ', res);
      this.ngOnInit();
      this.toasterService.popAsync('success', `Defect Profile ${res.name} successfully created!`);
      event.confirm.resolve();
    }, err=>{
      this.toasterService.popAsync('error', `Error while creating Defect Profile`, 'Make sure the name is unique!');
      event.confirm.reject();
      return;

    });
  }

  onEditConfirm(event: any) {
    this.defectProfilesService.updateDefectProfile(event.data._id, event.newData)
      .subscribe(res => {
        console.log('edit result: ', res);
        this.toasterService.popAsync('success', `Defect Profile ${res.name} successfully edited!`);
        event.confirm.resolve();
      }, err=>{
        this.toasterService.popAsync('error', `Error while editing Defect Profile`, 'Make sure the name is unique!');
        event.confirm.reject();
        return;
      })
  }

  onDeleteConfirm(event: any) {
    this.modalService.openDialog(this.viewRef, {
      title: 'Confirm',
      childComponent: SimpleModalComponent,
      data: {
        text: 'Are you sure you want to delete this Defect Profile?'
      },
      settings: {
        closeButtonClass: 'close theme-icon-close'
      },
      actionButtons: [
        {
          text: 'YES', onAction: () => new Promise((resolve:any) => {
            this.defectProfilesService.deleteDefectProfile(event.data._id)
              .subscribe(res => {
                console.log('delete result: ', res);
                resolve();
                this.toasterService.popAsync('success', `Defect Profile successfully deleted!`);
                event.confirm.resolve();
              }, err=>{
                this.toasterService.popAsync('error', `Error while deleting Defect Profile`, 'Make sure you are connected to the Internet');
                resolve();
                event.confirm.reject();
                return;
              })
          })
        },
        {text: 'No', onAction: () => new Promise(resolve=>{
            resolve();
          })}
      ],

    });
  }
}
