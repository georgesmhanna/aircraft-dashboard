import { Component, OnInit, ViewContainerRef } from '@angular/core';
import {UserService} from "../../@core/data/users.service";
import {RolesService} from "../../@core/data/roles.service";
import * as moment from "moment";
import {ToasterService} from "angular2-toaster";
import 'style-loader!angular2-toaster/toaster.css';
import {ModalDialogService, SimpleModalComponent} from "ngx-modal-dialog";
import {Router} from "@angular/router";


@Component({
  selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  users: any;
  settings: any;
  source: any;
  roles: any[] = [];

  constructor(private usersService: UserService,
              private rolesService: RolesService,
              private toastService: ToasterService,
              private modalService: ModalDialogService,
              private viewRef: ViewContainerRef,
              private router: Router){ }

  ngOnInit() {
    this.roles = [];
    this.rolesService.getRoles()
      .subscribe(roles=>{
        roles.forEach(role=>{
          this.roles.push({
            value: role._id,
            title: role.name
          });
        });
        this.settings = {
          add: {
            addButtonContent: '<i class="fa fa-plus"></i>',
            createButtonContent: '<i class="nb-checkmark"></i>',
            cancelButtonContent: '<i class="nb-close"></i>',
            confirmCreate: true
          },
          edit: {
            editButtonContent: '<i class="nb-edit"></i>',
            saveButtonContent: '<i class="nb-checkmark"></i>',
            cancelButtonContent: '<i class="nb-close"></i>',
            confirmSave: true
          },
          delete: {
            deleteButtonContent: '<i class="nb-trash"></i>',
            confirmDelete: true,
          },
          columns: {

            firstName: {
              title: 'First Name',
              type: 'string',
            },
            lastName: {
              title: 'Last Name',
              type: 'string'
            },
            email: {
              title: 'Email',
              type: 'string'
            },
            lastLogin: {
              title: 'Last Login',
              type: 'html',
              editable: false,
              filter: false,
              addable: false,
              valuePrepareFunction: (value) => {
                let returnValue = moment(value).format('MMMM DD YYYY, h:mm:ss a');
                return returnValue!=='Invalid date'?  returnValue: '';
              }
            },
            role: {
              title: 'Role',
              editor: {
                type: 'list',
                config: {
                  list: this.roles
                }
              },
              filter: {
                type: 'list',
                config: {
                  list: this.roles
                }
              },
              valuePrepareFunction: (value) => {
                if (this.roles) {
                  return this.roles
                    .filter(role => role.value === value)
                    .map(x => x.title)[0];
                }
              }
            }
          },
          actions: {
            position: 'right',
            add: false,
            edit: true,
            delete: true
          }
        };
        this.source = this.usersService.getUsers()
          .subscribe(users=>{
            this.users = users;
          }, err=>{
            this.toastService.popAsync('error','Error', 'Could not fetch list of users. Please try to reload.');
          });
      }, err=>{
        this.toastService.popAsync('error','Error', 'Could not fetch list of roles. Please try to reload.');
      });
  }

  onEditConfirm(event: any) {
    this.usersService.updateUser(event.data._id, event.newData)
      .subscribe(res => {
        console.log('edit result: ', res);
        this.toastService.popAsync('success', `User ${res.firstName} ${res.lastName} successfully edited!`);
        event.confirm.resolve();
      }, err=>{
        this.toastService.popAsync('error', `Error while editing user`, 'Make sure the email address is unique and that it is valid!');
        event.confirm.reject();
        return;
      })
  }

  // onEditConfirm(event: any) {
  //   console.log(event);
  //   this.toastService.popAsync('default', 'titile');
  // //   let newrole = this.roles.filter(x=>x.value===event.newData.role);
  // //   event.newData.role = {
  // //     '._id' : newrole[0].value,
  // //     name : newrole[0].title
  // // };
  //   this.settings.columns.role.editor.config.list = this.roles;
  //   event.confirm.resolve();
  // }

  onDeleteConfirm(event: any) {
    this.modalService.openDialog(this.viewRef, {
      title: 'Confirm',
      childComponent: SimpleModalComponent,
      data: {
        text: 'Are you sure you want to delete this user?'
      },
      settings: {
        closeButtonClass: 'close theme-icon-close'
      },
      actionButtons: [
        {
          text: 'YES', onAction: () => new Promise((resolve:any) => {
            this.usersService.deleteUser(event.data._id)
              .subscribe(res => {
                console.log('delete result: ', res);
                resolve();
                this.toastService.popAsync('success', `User successfully deleted!`);
                event.confirm.resolve();
              }, err=>{
                this.toastService.popAsync('error', `Error while deleting user`, 'Make sure you are connected to the Internet');
                resolve();
                event.confirm.reject();
                return;
              })
          })
        },
        {text: 'No', onAction: () => new Promise(resolve=>{
            resolve();
          })}
      ],

    });
  }

  // onCreateConfirm(event: any) {
  //   console.log(event);
  //   this.usersService.createUser(event.newData).subscribe(res=>{
  //     this.ngOnInit();
  //     this.toastService.popAsync('success', `User ${res.firstName} ${res.lastName} successfully created!`);
  //     event.confirm.resolve();
  //   }, err=>{
  //     this.toastService.popAsync('error', `Error while creating user`, 'Make sure the email address is unique and that it is valid!');
  //     event.confirm.reject();
  //     return;
  //   });
  // }

  goToCreate() {
    this.router.navigate(['pages/createUser']);
  }
}
