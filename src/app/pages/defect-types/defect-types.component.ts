import { Component, OnInit, ViewContainerRef } from '@angular/core';
import {ToasterService} from "angular2-toaster";
import 'style-loader!angular2-toaster/toaster.css';
import {ModalDialogService, SimpleModalComponent} from "ngx-modal-dialog";
import {DefectProfilesService} from "../../@core/data/defect-profiles.service";
import {DefectTypesService} from "../../@core/data/defect-types.service";


@Component({
  selector: 'defect-types',
  templateUrl: './defect-types.component.html',
  styleUrls: ['./defect-types.component.scss']
})
export class DefectTypesComponent implements OnInit {

  defectTypes: any;
  settings: any;
  source: any;
  profiles: any[] = [];

  constructor(private defectProfilesService: DefectProfilesService,
              private defectTypesService: DefectTypesService,
              private toastService: ToasterService,
              private modalService: ModalDialogService,
              private viewRef: ViewContainerRef) { }

  ngOnInit() {
    this.profiles = [];
    this.defectProfilesService.getDefectProfiles()
      .subscribe(profiles=>{
        profiles.forEach(profile=>{
          this.profiles.push({
            value: profile._id,
            title: profile.name
          });
        });
        this.settings = {
          add: {
            addButtonContent: '<i class="fa fa-plus"></i>',
            createButtonContent: '<i class="nb-checkmark"></i>',
            cancelButtonContent: '<i class="nb-close"></i>',
            confirmCreate: true
          },
          edit: {
            editButtonContent: '<i class="nb-edit"></i>',
            saveButtonContent: '<i class="nb-checkmark"></i>',
            cancelButtonContent: '<i class="nb-close"></i>',
            confirmSave: true
          },
          delete: {
            deleteButtonContent: '<i class="nb-trash"></i>',
            confirmDelete: true,
          },
          columns: {

            name: {
              title: 'Name',
              type: 'string',
            },
            defectProfile: {
              title: 'Defect Profile',
              editor: {
                type: 'list',
                config: {
                  list: this.profiles
                }
              },
              filter: {
                type: 'list',
                config: {
                  list: this.profiles
                }
              },
              valuePrepareFunction: (value) => {
                if (this.profiles) {
                  return this.profiles
                    .filter(profile => profile.value === value)
                    .map(x => x.title)[0];
                }
              }
            }
          },
          actions: {
            position: 'right'
          }
        };
        this.source = this.defectTypesService.getDefectTypes()
          .subscribe(defectTypes=>{
            this.defectTypes = defectTypes;
          }, err=>{
            this.toastService.popAsync('error','Error', 'Could not fetch list of defect types. Please try to reload.');
          });
      }, err=>{
        this.toastService.popAsync('error','Error', 'Could not fetch list of defect types. Please try to reload.');
      });
  }

  onEditConfirm(event: any) {
    this.defectTypesService.updateDefectType(event.data._id, event.newData)
      .subscribe(res => {
        console.log('edit result: ', res);
        this.toastService.popAsync('success', `Defect Type  ${res.name} successfully edited!`);
        event.confirm.resolve();
      }, err=>{
        this.toastService.popAsync('error', `Error while editing Defect Type`, 'Make sure the name is unique!');
        event.confirm.reject();
        return;
      })
  }

  onDeleteConfirm(event: any) {
    this.modalService.openDialog(this.viewRef, {
      title: 'Confirm',
      childComponent: SimpleModalComponent,
      data: {
        text: 'Are you sure you want to delete this Defect Type?'
      },
      settings: {
        closeButtonClass: 'close theme-icon-close'
      },
      actionButtons: [
        {
          text: 'YES', onAction: () => new Promise((resolve:any) => {
            this.defectTypesService.deleteDefectType(event.data._id)
              .subscribe(res => {
                console.log('delete result: ', res);
                resolve();
                this.toastService.popAsync('success', `Defect Type successfully deleted!`);
                event.confirm.resolve();
              }, err=>{
                this.toastService.popAsync('error', `Error while deleting Defect Type`, 'Make sure you are connected to the Internet');
                resolve();
                event.confirm.reject();
                return;
              })
          })
        },
        {text: 'No', onAction: () => new Promise(resolve=>{
            resolve();
          })}
      ],

    });
  }

  onCreateConfirm(event: any) {
    console.log(event);
    this.defectTypesService.createDefectType(event.newData).subscribe(res=>{
      this.ngOnInit();
      this.toastService.popAsync('success', `Defect Type ${res.name} successfully created!`);
      event.confirm.resolve();
    }, err=>{
      this.toastService.popAsync('error', `Error while creating Defect Type`, 'Make sure the name is unique');
      event.confirm.reject();
      return;
    });
  }

}
