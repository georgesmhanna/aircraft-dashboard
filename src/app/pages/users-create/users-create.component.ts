import {Component, Inject, OnInit} from '@angular/core';
import {NB_AUTH_OPTIONS, NbAuthResult, NbAuthService, NbAuthSocialLink} from "@nebular/auth";
import {RolesService} from "../../@core/data/roles.service";
import {getDeepFromObject} from "@nebular/auth/helpers";
import {Router} from "@angular/router";
import * as CryptoJS from 'crypto-js/crypto-js';
import {UserService} from "../../@core/data/users.service";
import {ToasterService} from "angular2-toaster";

@Component({
  selector: 'users-create',
  templateUrl: './users-create.component.html',
  styleUrls: ['./users-create.component.scss']
})
export class UsersCreateComponent implements OnInit {


  ngOnInit(): void {
  }

  redirectDelay: number = 0;
  showMessages: any = {};
  provider: string = '';

  submitted = false;
  errors: string[] = [];
  messages: string[] = [];
  user: any = {};
  roles: any[];
  lepassword: string;
  confirmPassword: string;
  socialLinks: NbAuthSocialLink[] = [];

  constructor(protected service: NbAuthService,
              @Inject(NB_AUTH_OPTIONS) protected config = {},
              protected router: Router,
              private rolesService: RolesService,
              private userService: UserService,
              private toastService: ToasterService) {

    this.rolesService.getRoles()
      .subscribe(roles=>{
        this.roles = roles;
      });

    this.redirectDelay = this.getConfigValue('forms.register.redirectDelay');
    this.showMessages = this.getConfigValue('forms.register.showMessages');
    this.provider = this.getConfigValue('forms.register.provider');
    this.socialLinks = this.getConfigValue('forms.login.socialLinks');
  }

  register(): void {

    this.errors = this.messages = [];
    this.submitted = true;

    this.user.password = CryptoJS.SHA256(this.lepassword).toString();
    this.user.confirmPassword = CryptoJS.SHA256(this.confirmPassword).toString();
    // this.service.register(this.provider, this.user).subscribe((result: NbAuthResult) => {
    this.userService.createUser(this.user).subscribe(result => {
      // this.submitted = false;
      // if (result.isSuccess()) {
      //   this.messages = result.getMessages();
      // } else {
      //   this.errors = result.getErrors();
      // }
      this.toastService.popAsync('success',
        `User ${result.firstName} ${result.lastName} successfully created!`);


        setTimeout(() => {
          return this.router.navigate(['pages/users']);
        }, 1000);

    },
      err=>{
        this.submitted = false;
        this.toastService.popAsync('error', `Error while creating user`, 'Make sure the email address is unique and that it is valid!');
        return;
      });
  }

  getConfigValue(key: string): any {
    return getDeepFromObject(this.config, key, null);
  }
}
