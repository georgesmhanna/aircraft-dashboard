import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {ToasterService} from "angular2-toaster";
import 'style-loader!angular2-toaster/toaster.css';
import {ModalDialogService, SimpleModalComponent} from "ngx-modal-dialog";
import {AircraftModelsService} from "../../@core/data/aircraft-models.service";


@Component({
  selector: 'aircraft-models',
  templateUrl: './aircraft-models.component.html',
  styleUrls: ['./aircraft-models.component.scss']
})
export class AircraftModelsComponent implements OnInit {

  aircraftModels: any;
  settings: any;
  source: any;

  constructor(private aircraftModelService: AircraftModelsService,
              private toasterService: ToasterService,
              private modalService: ModalDialogService, private viewRef: ViewContainerRef) {


    this.settings = {
      add: {
        addButtonContent: '<i class="fa fa-plus"></i>',
        createButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
        confirmCreate: true
      },
      edit: {
        editButtonContent: '<i class="nb-edit"></i>',
        saveButtonContent: '<i class="nb-checkmark"></i>',
        cancelButtonContent: '<i class="nb-close"></i>',
        confirmSave: true
      },
      delete: {
        deleteButtonContent: '<i class="nb-trash" mwlConfirmationPopover></i>',
        confirmDelete: true,
      },
      columns: {

        name: {
          title: 'Name',
          type: 'string',
          width: '45%'
        },
        registrationNo: {
          title: 'Registration Number',
          type: 'string',
          width: '45%'
        }
      },
      actions: {
        position: 'right'
      }
    };

    this.source = this.aircraftModelService.getAircraftModels();
  }
    ngOnInit() {
    this.aircraftModelService.getAircraftModels()
      .subscribe(aircraftModels=>{
        this.aircraftModels = aircraftModels;
      }, err=>{
        this.toasterService.popAsync('error','Error', 'Could not fetch list of aircraft models. Please try to reload.');
      })
  }

  onCreateConfirm(event: any) {
    console.log(event);
    this.aircraftModelService.createAircraftModel(event.newData)
      .subscribe(res=>{
      this.ngOnInit();
      this.toasterService.popAsync('success', `Aircraft Model ${res.name} successfully created!`);
      event.confirm.resolve();
    }, err=>{
      this.toasterService.popAsync('error', `Error while creating Aircraft Model`, 'Make sure that the name is unique!');
      event.confirm.reject();
      return;
    });
  }

  onEditConfirm(event: any) {
    this.aircraftModelService.updateAircraftModel(event.data._id, event.newData)
      .subscribe(res => {
        this.toasterService.popAsync('success', `Aircraft Model ${res.name} successfully edited!`);
        event.confirm.resolve();
      }, err=>{
        this.toasterService.popAsync('error', `Error while editing Aircraft Model`, 'Make sure that the name is unique!');
        event.confirm.reject();
        return;
      })
  }

  onDeleteConfirm(event: any) {
    this.modalService.openDialog(this.viewRef, {
      title: 'Confirm',
      childComponent: SimpleModalComponent,
      data: {
        text: 'Are you sure you want to delete this Aircraft Model?'
      },
      settings: {
        closeButtonClass: 'close theme-icon-close'
      },
      actionButtons: [
        {
          text: 'YES', onAction: () => new Promise((resolve:any) => {
            this.aircraftModelService.deleteAircraftModel(event.data._id)
              .subscribe(res => {
                resolve();
                this.toasterService.popAsync('success', `Aircraft Model successfully deleted!`);
                event.confirm.resolve();
              }, err=>{
                this.toasterService.popAsync('error', `Error while deleting Aircraft Model`, 'Make sure you are connected to the Internet');
                resolve();
                event.confirm.reject();
                return;
              })
          })
        },
        {text: 'No', onAction: () => new Promise(resolve=>{
            resolve();
          })}
      ],

    });
  }

}
