import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AircraftSectionsComponent } from './aircraft-sections.component';

describe('AircraftSectionsComponent', () => {
  let component: AircraftSectionsComponent;
  let fixture: ComponentFixture<AircraftSectionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AircraftSectionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AircraftSectionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
