import { Component, OnInit, ViewContainerRef } from '@angular/core';

import {Toast, ToasterService} from "angular2-toaster";
import 'style-loader!angular2-toaster/toaster.css';
import {ModalDialogService, SimpleModalComponent} from "ngx-modal-dialog";
import {AircraftSectionsService} from "../../@core/data/aircraft-sections.service";
import {AircraftsService} from "../../@core/data/aircrafts.service";
import {DefectProfilesService} from "../../@core/data/defect-profiles.service";


@Component({
  selector: 'aircraft-sections',
  templateUrl: './aircraft-sections.component.html',
  styleUrls: ['./aircraft-sections.component.scss']
})
export class AircraftSectionsComponent implements OnInit {

  aircraftSections: any;
  settings: any;
  source: any;
  aircrafts: any[] = [];
  parents: any[] = [{title: 'No Parent', value: null}];
  defectProfiles: any[] = [];

  constructor(private aircraftSectionsService: AircraftSectionsService,
              private aircraftsService: AircraftsService,
              private defectProfilesService: DefectProfilesService,
              private toastService: ToasterService,
              private modalService: ModalDialogService,
              private viewRef: ViewContainerRef) { }



  ngOnInit() {
    // Get list of aircrafts:
    this.aircrafts = [];
    this.aircraftsService.getAircrafts()
      .subscribe(aircrafts=>{
        aircrafts.forEach(aircraft=>{
          this.aircrafts.push({
            value: aircraft._id,
            title: aircraft.name
          });
        });
        console.log(this.aircrafts);
        // Get list of aircraft sections to be chosen as a parent:
        this.parents = [{title: 'No Parent', value: null}];
        this.aircraftSectionsService.getAircraftSections()
          .subscribe(parents=>{
            parents.forEach(parent=>{
              this.parents.push({
                value: parent._id,
                title: parent.name
              });
            });
            // Get list of defect profiles:
            this.defectProfiles = [];
            this.defectProfilesService.getDefectProfiles()
              .subscribe(defectProfiles=>{
                defectProfiles.forEach(defectProfile=>{
                  this.defectProfiles.push({
                    value: defectProfile._id,
                    title: defectProfile.name
                  });
                });
                // Assign this.settings for the datatable

                this.settings = {
                  add: {
                    addButtonContent: '<i class="fa fa-plus"></i>',
                    createButtonContent: '<i class="nb-checkmark"></i>',
                    cancelButtonContent: '<i class="nb-close"></i>',
                    confirmCreate: true
                  },
                  edit: {
                    editButtonContent: '<i class="nb-edit"></i>',
                    saveButtonContent: '<i class="nb-checkmark"></i>',
                    cancelButtonContent: '<i class="nb-close"></i>',
                    confirmSave: true
                  },
                  delete: {
                    deleteButtonContent: '<i class="nb-trash"></i>',
                    confirmDelete: true,
                  },
                  columns: {

                    name: {
                      title: 'Name',
                      type: 'string',
                    },
                    aircraft: {
                      title: 'Aircraft',
                      editor: {
                        type: 'list',
                        config: {
                          list: this.aircrafts
                        }
                      },
                      filter: {
                        type: 'list',
                        config: {
                          list: this.aircrafts
                        }
                      },
                      valuePrepareFunction: (value) => {
                        if (this.aircrafts) {
                          return this.aircrafts
                            .filter(aircraft => aircraft.value === value)
                            .map(x => x.title)[0];
                        }
                      }
                    },
                    parent: {
                      title: 'Parent Section',
                      editor: {
                        type: 'list',
                        config: {
                          list: this.parents
                        }
                      },
                      filter: {
                        type: 'list',
                        config: {
                          list: this.parents
                        }
                      },
                      valuePrepareFunction: (value) => {
                        if(value==='null')
                          return 'No Parent';
                        if (this.parents) {
                          return this.parents
                            .filter(parent => parent.value === value)
                            .map(x => x.title)[0];
                        }
                      }
                    },
                    defectProfile: {
                      title: 'Defect Profile',
                      editor: {
                        type: 'list',
                        config: {
                          list: this.defectProfiles
                        }
                      },
                      filter: {
                        type: 'list',
                        config: {
                          list: this.defectProfiles
                        }
                      },
                      valuePrepareFunction: (value) => {
                        if (this.defectProfiles) {
                          return this.defectProfiles
                            .filter(defectProfile => defectProfile.value === value)
                            .map(x => x.title)[0];
                        }
                      }
                    }
                  },
                  actions: {
                    position: 'right'
                  }
                };

                this.aircraftSections = [];
                this.source = this.aircraftSectionsService.getAircraftSections()
                  .subscribe(aircraftSections=>{
                    aircraftSections.forEach(as=>{
                      if(!as.parent)
                        as.parent = 'null';
                    });
                    this.aircraftSections = aircraftSections;
                  }, err=>{
                    this.toastService.popAsync('error','Error', 'Could not fetch list of aircraft sections. Please try to reload.');
                  });
              }, err1=>{
                this.toastService.popAsync('error','Error', 'Could not fetch list of defect profiles. Please try to reload.');
              });
          }, err2=>{
            this.toastService.popAsync('error','Error', 'Could not fetch list of parent aircraft sections. Please try to reload.');
          });
      }, err3=>{
        this.toastService.popAsync('error','Error', 'Could not fetch list of aircrafts. Please try to reload.');
      });
  }

  onEditConfirm(event: any) {
    if(event.newData.parent && event.newData.parent==='null'){
       event.newData.parent = null;
    }
    this.aircraftSectionsService.updateAircraftSection(event.data._id, event.newData)
      .subscribe(res => {
        console.log('edit result: ', res);
        this.ngOnInit();
        this.toastService.popAsync('success', `Aircraft Section ${res.name} successfully edited!`);
        event.confirm.resolve();
      }, err=>{
        this.toastService.popAsync('error', `Error while editing Aircraft Section`, 'Make sure the name is unique!');
        event.confirm.reject();
        return;
      })
  }

  onDeleteConfirm(event: any) {
    this.modalService.openDialog(this.viewRef, {
      title: 'Confirm',
      childComponent: SimpleModalComponent,
      data: {
        text: 'Are you sure you want to delete this Aircraft Section?'
      },
      settings: {
        closeButtonClass: 'close theme-icon-close'
      },
      actionButtons: [
        {
          text: 'YES', onAction: () => new Promise((resolve:any) => {
            this.aircraftSectionsService.deleteAircraftSection(event.data._id)
              .subscribe(res => {
                console.log('delete result: ', res);
                resolve();
                this.ngOnInit();
                this.toastService.popAsync('success', `Aircraft Section successfully deleted!`);
                event.confirm.resolve();
              }, err=>{
                this.toastService.popAsync('error', `Error while deleting Aircraft Section`, 'Make sure you are connected to the Internet');
                resolve();
                event.confirm.reject();
                return;
              })
          })
        },
        {text: 'No', onAction: () => new Promise(resolve=>{
            resolve();
          })}
      ],
    });
  }

  onCreateConfirm(event: any) {
    console.log(event);
    if(event.newData.parent && event.newData.parent==='null'){
       event.newData.parent = null;
    }
    console.log(event);
    this.aircraftSectionsService.createAircraftSection(event.newData)
      .subscribe(res=>{
      this.ngOnInit();
      this.toastService.popAsync('success', `Aircraft Section ${res.name} successfully created!`);
      event.confirm.resolve();
    }, err=>{
      this.toastService.popAsync('error', `Error while creating Aircraft Section`, 'Make sure the name is unique.');
      event.confirm.reject();
      return;
    });
  }

}
